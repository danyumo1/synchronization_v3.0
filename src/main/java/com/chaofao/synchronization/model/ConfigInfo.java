package com.chaofao.synchronization.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Chaofan at 2018-12-25 14:04
 * email:chaofan2685@qq.com
 **/
public class ConfigInfo implements Serializable {

    //同步类型
    private String type;

    //同步目录
    private Map<String,String> directory;

    //需要同步的文件类型(为空则全部同步)
    private List<String> whitelist;

    //对比两个文件是否一致的算法【MD5(两个文件MD5值一致即为同一文件)/LENGTH(字节长度一致。。。)/NAME(名称相同。。。)】
    private String arithmetic;

    //回收站路径（为空则为关闭回收站）
    private String recycle;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getDirectory() {
        return directory;
    }

    public void setDirectory(Map<String, String> directory) {
        this.directory = directory;
    }

    public List<String> getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(List<String> whitelist) {
        this.whitelist = whitelist;
    }

    public String getArithmetic() {
        return arithmetic;
    }

    public void setArithmetic(String arithmetic) {
        this.arithmetic = arithmetic;
    }

    public String getRecycle() {
        return recycle;
    }

    public void setRecycle(String recycle) {
        this.recycle = recycle;
    }
}
