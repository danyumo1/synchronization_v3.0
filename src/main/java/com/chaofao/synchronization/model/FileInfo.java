package com.chaofao.synchronization.model;

import cn.hutool.crypto.digest.DigestUtil;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Chaofan at 2018-12-26 14:51
 * email:chaofan2685@qq.com
 **/
public class FileInfo implements Serializable {

    private String type;

    private String arithmetic;

    private List<String> path;

    private Long length;

    private String md5;

    public FileInfo() {
    }

    public FileInfo(File file, File destFile, Boolean isFolder){
        if (file.exists()){
            try {
                if (isFolder){
                    this.type = "FOLDER";
                }else {
                    this.type = "FILE";
                    this.length = file.length();
                    this.md5 = DigestUtil.md5Hex(file);
                }
                this.path = new ArrayList<>();
                path.add(destFile.getCanonicalPath());
                path.add(file.getCanonicalPath());
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public FileInfo(File file, File destFile, String arithmetic) {
        if (file.exists()){
            try {
                this.type = "FILE";
                this.path = new ArrayList<>();
                this.arithmetic = arithmetic;
                path.add(destFile.getCanonicalPath());
                path.add(file.getCanonicalPath());
                switch (arithmetic.toUpperCase()){
                    case "LENGTH":
                        this.length = file.length();
                        break;
                    case "MD5":
                        this.md5 = DigestUtil.md5Hex(file);
                        break;
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        FileInfo a = (FileInfo)o;
        for (String p : a.getPath()){
            if (!path.contains(p)){
                return false;
            }
        }
        if (!type.equals(a.getType())){
            return false;
        }
        if (arithmetic != null){
            switch (arithmetic.toUpperCase()){
                case "LENGTH":
                    return length.longValue() == a.getLength().longValue();
                case "MD5":
                    return md5.equals(a.getMd5());
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(arithmetic, path, length, md5);
    }

    public List<String> getPath() {
        return path;
    }

    public void setPath(List<String> path) {
        this.path = path;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getArithmetic() {
        return arithmetic;
    }

    public void setArithmetic(String arithmetic) {
        this.arithmetic = arithmetic;
    }
}
