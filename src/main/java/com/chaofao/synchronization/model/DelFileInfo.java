package com.chaofao.synchronization.model;


import cn.hutool.core.date.DateTime;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Chaofan at 2018-12-26 14:51
 * email:chaofan2685@qq.com
 **/
public class DelFileInfo implements Serializable {

    private File delFile;

    private String type;

    private String date;

    public DelFileInfo() {
    }

    public DelFileInfo(File delFile,String type) {
        this.delFile = delFile;
        this.type = type;
        this.date = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public File getDelFile() {
        return delFile;
    }

    public void setDelFile(File delFile) {
        this.delFile = delFile;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
