package com.chaofao.synchronization.model;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Chaofan at 2018-12-27 16:51
 * email:chaofan2685@qq.com
 **/
public class Msg implements Serializable {

    private String type;
    private String state;
    private String message;
    private String file;

    public static Msg build(String type, String state, File file, String message){
        return new Msg(type, state,file, message);
    }

    public Msg() {
    }

    public Msg(String type, String state, File file, String message) {
        this.type = type;
        this.state = state;
        this.message = message;
        this.file = file==null?null:file.toString();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
