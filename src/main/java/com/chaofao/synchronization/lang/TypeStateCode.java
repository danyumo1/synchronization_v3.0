package com.chaofao.synchronization.lang;

/**
 * Created by Chaofan at 2018-12-27 16:59
 * email:chaofan2685@qq.com
 **/
public interface TypeStateCode {

    String MSGTYPE_FOLDER = "FOLDER";
    String MSGTYPE_FILE = "FILE";
    String MSGTYPE_SYNC = "SYNC";
    String MSGSTATE_BACKUP = "BACKUP";
    String MSGSTATE_DELETE = "DELETE";
    String MSGSTATE_ERROR = "ERROR";

}
