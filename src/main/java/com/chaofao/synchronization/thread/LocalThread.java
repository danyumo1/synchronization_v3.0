package com.chaofao.synchronization.thread;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ZipUtil;
import com.chaofao.synchronization.common.ExistFileFilter;
import com.chaofao.synchronization.common.SaveInfoFileFilter;
import com.chaofao.synchronization.lang.TypeStateCode;
import com.chaofao.synchronization.model.ConfigInfo;
import com.chaofao.synchronization.model.DelFileInfo;
import com.chaofao.synchronization.model.FileInfo;
import com.chaofao.synchronization.model.Msg;
import com.chaofao.synchronization.util.Fileutil;
import com.chaofao.synchronization.web.SyncWebsocket;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Chaofan at 2018-12-26 13:13
 * email:chaofan2685@qq.com
 **/
public class LocalThread extends Thread  {

    private SyncWebsocket websocket;
    private ConfigInfo config;
    private List<FileInfo> fileInfos;
    private Map<String,DelFileInfo> delFileInfoMap;

    public LocalThread(SyncWebsocket websocket, ConfigInfo config){
        this.websocket = websocket;
        this.config = config;
        this.fileInfos = Fileutil.getFileInfos();
        this.delFileInfoMap = Fileutil.getDelFileInfos();
    }

    @Override
    public void run() {
        for (Map.Entry<String,String> entry : config.getDirectory().entrySet()) {
            String srcPath = entry.getKey().replace("/","\\");
            File srcFile = new File(srcPath);
            String destPath = entry.getValue().replace("/","\\");
            File destFile = new File(destPath);
            String message = srcPath+"<--->"+destPath;
            websocket.sendMessage(Msg.build(TypeStateCode.MSGTYPE_SYNC,null,null,message));
            try {
                for (FileInfo info : fileInfos){
                    List<String> list = info.getPath();
                    File fileL = FileUtil.file(list.get(0));
                    File fileR = FileUtil.file(list.get(1));
                    if (!(fileL.exists() && fileR.exists())){
                        delFile(fileL);
                        delFile(fileR);
                    }
                }
                Fileutil.setDelFileInfos();
                //遍历文件进行复制
                FileUtils.copyDirectory(srcFile,destFile,new ExistFileFilter(destPath,config.getWhitelist(),config.getArithmetic(),websocket,fileInfos),true);
                FileUtils.copyDirectory(destFile,srcFile,new ExistFileFilter(srcPath,config.getWhitelist(),config.getArithmetic(),websocket,fileInfos),true);
                SyncWebsocket.errorSyncList.clear();
                FileUtils.copyDirectory(srcFile,destFile,new SaveInfoFileFilter(destPath,config.getArithmetic(),fileInfos,config.getWhitelist()),true);
                Fileutil.setFileInfos();
                websocket.sendMessage(Msg.build(TypeStateCode.MSGTYPE_SYNC,null,null,message+"备份完成"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void delFile(File file){
        String typestatecode = TypeStateCode.MSGTYPE_FILE;
        if (file.exists()){
            if (StrUtil.isNotEmpty(config.getRecycle())){
                String recycle = config.getRecycle().endsWith("/")?config.getRecycle():config.getRecycle()+"/";
                if (file.isDirectory()){
                    recycle = recycle.replace("/","\\")+UUID.randomUUID()+".zip";
                    ZipUtil.zip(file.getPath(),recycle);
                    typestatecode = TypeStateCode.MSGTYPE_FOLDER;
                }else {
                    recycle = recycle.replace("/","\\")+UUID.randomUUID();
                    File delFile = FileUtil.file(recycle);
                    FileUtil.copy(file,delFile,false);
                }
                FileUtil.del(file);
                DelFileInfo delFileInfo = new DelFileInfo(file,typestatecode);
                delFileInfoMap.put(recycle,delFileInfo);
                websocket.sendMessage(Msg.build(typestatecode,TypeStateCode.MSGSTATE_DELETE,file,"删除成功"));
            }
        }
    }

}
