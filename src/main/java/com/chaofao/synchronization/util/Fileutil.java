package com.chaofao.synchronization.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chaofao.synchronization.model.ConfigInfo;
import com.chaofao.synchronization.model.DelFileInfo;
import com.chaofao.synchronization.model.FileInfo;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Chaofan at 2018/7/30 20:43
 * email:chaofan2685@qq.com
 **/
public class Fileutil {

    public static List<FileInfo> fileInfos = new ArrayList<>();

    public static Map<String,DelFileInfo> delFileInfoMap = new HashMap<>();;

    public static ConfigInfo getConfig (){
        try{
            Class aClass = new Object(){
                public Class getClassByThis(){
                    return this.getClass();
                }
            }.getClass();
            String path = new ApplicationHome(aClass).getSource().getParentFile().toString();
            File jsonFile= ResourceUtils.getFile(path+"\\configuration.json");
            String json = FileUtils.readFileToString(jsonFile, Charsets.UTF_8);
            ConfigInfo config = JSON.parseObject(json,ConfigInfo.class);
            return config;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static List<FileInfo> getFileInfos (){
        try{
            Class aClass = new Object(){
                public Class getClassByThis(){
                    return this.getClass();
                }
            }.getClass();
            String path = new ApplicationHome(aClass).getSource().getParentFile().toString();
            File jsonFile= ResourceUtils.getFile(path+"\\fileInfos.data");
            if (!jsonFile.exists()){
                jsonFile.createNewFile();
            }
            String json = FileUtils.readFileToString(jsonFile, Charsets.UTF_8);
            List<FileInfo> fileInfos = JSON.parseArray(json, FileInfo.class);
            if (fileInfos == null){
                return new ArrayList<>();
            }
            return fileInfos;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void setFileInfos(){
        String json = JSON.toJSONString(fileInfos);
        try{
            Class aClass = new Object(){
                public Class getClassByThis(){
                    return this.getClass();
                }
            }.getClass();
            String path = new ApplicationHome(aClass).getSource().getParentFile().toString();
            File jsonFile= ResourceUtils.getFile(path+"\\fileInfos.data");
            if (!jsonFile.exists()){
                jsonFile.createNewFile();
            }
            PrintStream ps = new PrintStream(new FileOutputStream(jsonFile));
            ps.print(json);// 往文件里写入字符串
            fileInfos.clear();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Map<String,DelFileInfo> getDelFileInfos (){
        try{
            Class aClass = new Object(){
                public Class getClassByThis(){
                    return this.getClass();
                }
            }.getClass();
            String path = new ApplicationHome(aClass).getSource().getParentFile().toString();
            File jsonFile= ResourceUtils.getFile(path+"\\recycle.data");
            if (!jsonFile.exists()){
                jsonFile.createNewFile();
            }
            String json = FileUtils.readFileToString(jsonFile, Charsets.UTF_8);
            if (StrUtil.isEmpty(json)){
                return delFileInfoMap;
            }
            JSONObject jsonObject = JSON.parseObject(json);
            Map<String,Object> map = jsonObject.getInnerMap();
            for (Map.Entry<String,Object> entry : map.entrySet()){
                delFileInfoMap.put(entry.getKey(),((JSONObject)entry.getValue()).toJavaObject(DelFileInfo.class));
            }
            return delFileInfoMap;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void setDelFileInfos(){
        String json = JSON.toJSONString(delFileInfoMap);
        try{
            Class aClass = new Object(){
                public Class getClassByThis(){
                    return this.getClass();
                }
            }.getClass();
            String path = new ApplicationHome(aClass).getSource().getParentFile().toString();
            File jsonFile= ResourceUtils.getFile(path+"\\recycle.data");
            if (!jsonFile.exists()){
                jsonFile.createNewFile();
            }
            PrintStream ps = new PrintStream(new FileOutputStream(jsonFile));
            ps.print(json);// 往文件里写入字符串
            fileInfos.clear();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
