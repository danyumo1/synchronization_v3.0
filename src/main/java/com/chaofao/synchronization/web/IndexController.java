package com.chaofao.synchronization.web;

import com.chaofao.synchronization.model.ConfigInfo;
import com.chaofao.synchronization.util.Fileutil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created by Chaofan at 2018-12-14 15:57
 * email:chaofan2685@qq.com
 **/
@Controller
public class IndexController {

    @RequestMapping("/")
    public String index(Model model){
        ConfigInfo configInfo = Fileutil.getConfig();
        model.addAttribute("configInfo",configInfo);
        return "index";
    }

}
