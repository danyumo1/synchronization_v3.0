package com.chaofao.synchronization.web;

import com.alibaba.fastjson.JSON;
import com.chaofao.synchronization.model.ConfigInfo;
import com.chaofao.synchronization.model.Msg;
import com.chaofao.synchronization.thread.LocalThread;
import com.sun.applet2.AppletParameters;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Chaofan at 2018-12-25 13:35
 * email:chaofan2685@qq.com
 **/

@Component
@ServerEndpoint(value = "/sync/test")
public class SyncWebsocket {

    public static HashMap<String,String> errorSyncList = new HashMap<>();

    private Session session;

    /**
     * 连接建立成功调用的方法
     * @param session
     */
    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
    }


    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {

    }

    /**
     * 收到客户端消息后调用的方法
     * @param message
     */
    @OnMessage
    public void onMessage(String message){
        ConfigInfo config = JSON.parseObject(message, ConfigInfo.class);
        switch (config.getType()){
            case "FTP":
                break;
            case "LOCAL":
                new LocalThread(this,config).start();
                break;
        }
    }

    public void sendMessage(Msg msg){
        try {
            this.session.getBasicRemote().sendText(JSON.toJSONString(msg));
        }catch (IOException e){

        }
    }

}
